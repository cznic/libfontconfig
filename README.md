# libfontconfig

Package libfontconfig is a ccgo/v4 version of libfontconfig.a, a library
providing configuration, enumeration and substitution of fonts to other
programs.
